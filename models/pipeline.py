import sys
import time
import gevent
from gevent import Greenlet
from . import app
import traceback


class Pipeline(gevent.Greenlet):

    def __init__(self, name, queue):
        # listen to thoonk qeue list here
        self.ps_all = time.time()
        self.queue = queue
        self.inbox = app._pubSub.queue(self.queue)
        self.drafts = app._pubSub.queue('webPendingNotifications')
        self.name = name
        Greenlet.__init__(self)

    def receive(self, name, task):

        print("Worker %s recived task %s" % (name, task))

    def parse(self, data):

        raise NotImplemented('No ParserMixin used')

    def notify(self, data, parsed_data, computed_data):

        raise NotImplemented('No publisherMixin used')

    def publish(self, data, parsed_data):

        raise NotImplemented('No publisherMixin used')

    def updateQue(self, data):
        raise NotImplemented()

    def batch_handle(self, data):
        raise NotImplemented('No handle Mixin')

    def handle(self, data):
       raise NotImplemented('No handle Mixin')

    def _run(self):
        self.running = True

        while self.running:
            try:
                while True:
                    # decrements queue size by
                    # does a blocking get
                    task = self.inbox.get(timeout=1)
                    print('Worker %s got task %s' % (self.name, task))
                    ts = time.time()
                    print "invoking:"
                    try:
                        self.handle(task)
                    except Exception as e:
                        print traceback.print_exc()
                    print "should have handled"
                
                    gevent.sleep(1)
                    # print job.result
                    print('Took {}s'.format(time.time() - ts))
            except Exception as e:
                print e
            except KeyboardInterrupt:
                sys.exit(['Closing notification system now'])
