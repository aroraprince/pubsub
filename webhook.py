from flask import Flask, url_for, request, jsonify
from functools import wraps
import json
from thoonk import Pubsub
import gevent

app = Flask(__name__)


def check_auth(username, password):
    return username == 'admin' and password == '5UPP31253C1237'


def authenticate():
    message = {'message': "Authenticate."}
    resp = jsonify(message)

    resp.status_code = 401
    resp.headers['WWW-Authenticate'] = 'Basic realm="Example"'

    return resp


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return authenticate()

        elif not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated


def add_to_tetrx(data, queue):
    p = Pubsub(listen=False)
    j = p.queue(queue)
    data = json.dumps(data)
    j.put(str(data))
    return True


def process_data(data, queue):
    publisher = gevent.spawn(add_to_tetrx, data, queue)
    gevent.joinall([publisher])
    return publisher


@app.route('/')
def api_root():
    return 'Welcome'


@app.route('/publish', methods=['POST'])
def api_notify():
    print "here"
    if request.headers['Content-Type'] == 'application/json':
        # add message to que here
        # check if message contains notify_to and notification_from fields here
        if request.json:
            data = request.json
            ret_val = process_data(data, 'notify')
            print ret_val
            return jsonify(request.json)
        resp = jsonify(
            {"message": "Bad Request"}), 403, 'application/json'
        return resp
    else:
        resp = jsonify(
            {"message": "Bad Request"}), 403, 'application/json'
        return resp

if __name__ == '__main__':
    app.run(debug=True)
