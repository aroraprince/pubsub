from gevent import Greenlet
from requests.auth import HTTPBasicAuth
import requests
import json


baseUrl = 'http://localhost:5000'
auth = HTTPBasicAuth('admin', '5UPP31253C1237')


class Notify:

    """
     notify = Notify()
      notify.publish(client_id='3d91928b729b432ca22799cb909e2f1d',
                    author='3d91928b729b432ca22799cb909e2f1f',
                    message='HEllo Random %s!' % x, channel='franklyMe')
    """

    def __init__(self, notify_to=None, notify_from=None, message=None,
                 channel=None):
        self.client_id = notify_to
        self.author = notify_from
        self.message = message
        self.channel = channel
        # get user credentials

    @classmethod
    def publish(cls, message, channel, client_id, author):
        payload = {"message": message,
                   "channel": channel,
                   "client_id": client_id,
                   "author": author}
        payload = json.dumps(payload)
        headers = {'content-type': 'application/json'}
        r = requests.post(baseUrl+'/publish', data=payload, auth=auth, headers=headers)

        if r.status_code == requests.codes.ok:
            print(r.headers['content-type'])
            return True
        else:
            print r.headers
            return r.status_code



if __name__ == '__main__':
  notify = Notify()
  notify.publish(client_id='ch1',
                author='3d91928b729b432ca22799cb909e2f1f',
                message='HEllo Random %s!' % 'USER14', channel='ch1')
