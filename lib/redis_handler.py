import redis


class RedisHandler:

    def __init__(self, host='localhost', port='6379'):
        self.host = host
        self.port = port
        self.connection = self.get_connection()

    def get_connection(self):
        print "getting connection"
        return redis.StrictRedis(host=self.host, port=self.port, db='0')

    def key_exists(self, key):
        return self.connection.exists(key)

    def set_ttl(self, key, ttl=5):
        return self.connection.expire(key, ttl)

    def set_key(self, key, val):
        return self.connection.set(key, val)