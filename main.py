import gevent
import os
from mixins.json_parser import JSONParserMixin
from mixins.thoonk_listener import ThoonkQeueParserMixin
from mixins.http_publisher import HTTPPublisher
import multiprocessing
import gipc


class AsyncQuePublisher(JSONParserMixin, ThoonkQeueParserMixin,
                        HTTPPublisher):
     def handle(self, data):
        parsed_data = self.parse(data)
        publised_result = self.publish(data, parsed_data)
        ret_val = self.updateQue(data, publised_result)
        if ret_val is True:
            return data
        else:
            return 'Failed to Publishsh'


def process_start():
    print "starting processing"
    print "==================="
    print os.getpid()
    workers = [AsyncQuePublisher(x, 'notify') for x in xrange(1, 10)]
    c = 1
    for worker in workers:
        print "worker", c
        c+=1
        worker.start()
    gevent.joinall(workers)


def run_workers(target):
    """
    Creates processes * <limit>, running the given target
    function for each.
    """
    limit = multiprocessing.cpu_count() / 2
    for _ in range(limit):
        proc = gipc.start_process(target)
        proc.join()

if __name__ == '__main__':
    # No multi processing on micro t1
    #run_workers(process_start)
    process_start()
