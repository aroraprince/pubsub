### Mobile Messaging Server###


### How do I get set up? ###

* Build and install [nginx push stream](https://github.com/wandenberg/nginx-push-stream-module)
* Install redis
* Install python dependencies 
```
#!python

pip install -r requirements.txt
```

To run server: 
```
#!python

python main.py
```

To run web client:

```
#!python

python client.py
```

You can use client.html in front/ to emulate socket connection

To publish notifications
   
   Url : http://localhost:5000/publish

   request_json : {"message": "Hello!", "channel": "ch1", "client_id": "ch1"}