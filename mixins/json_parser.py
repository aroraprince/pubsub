from models.pipeline import Pipeline
import ujson
import json


class JSONParserMixin(Pipeline):

    def parse(self, data):
        return ujson.dumps(data)
