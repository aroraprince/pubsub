from models.pipeline import Pipeline
from lib.redis_handler import RedisHandler

import json
import ast
import requests
from datetime import datetime

socket_config = {'baseUrl': "http://localhost:9080/pub?id={user_id}"}


class HTTPPublisher(Pipeline):

    def publish(self, data, parsed_data):
        print "publishing"
        payload = parsed_data
        parsed = json.loads(payload)
        evald = ast.literal_eval(parsed)
        user_id = evald['client_id']
        print socket_config['baseUrl'].format(user_id=user_id)
        redis_handler = RedisHandler()
        try:
            # if key does not exist it has already been 5 sec
            if not redis_handler.key_exists(user_id):
                print parsed
                req = requests.post(socket_config['baseUrl'].format(user_id=user_id), data=parsed)
                redis_handler.set_key(user_id, 'abc')
                redis_handler.set_ttl(user_id, 5)
                print req.status_code
            else:
                print "duplicate"
            return True
        except Exception as e:
            print e
            return False
