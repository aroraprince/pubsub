from models.pipeline import Pipeline


class ThoonkQeueParserMixin(Pipeline):

    def updateQue(self, data, publised_result):
        if publised_result is True:
            print "Message delivered to Client"
            return True
        else:
            self.drafts.put(data)
            return False
